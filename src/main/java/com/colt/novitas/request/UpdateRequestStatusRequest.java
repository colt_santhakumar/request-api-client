package com.colt.novitas.request;

import java.io.Serializable;

public class UpdateRequestStatusRequest implements Serializable {

	private static final long serialVersionUID = 3255750633736015308L;

	private String status;
	private Integer statusCode;
	private String serviceId;
	private Float penaltyCharge;
	private String processInstanceId;
	private String hostAddress;
    private Integer bandwidth;
    
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	/**
	 * @return the processInstanceId
	 */
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	/**
	 * @param processInstanceId the processInstanceId to set
	 */
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	/**
	 * @return the hostAddress
	 */
	public String getHostAddress() {
		return hostAddress;
	}

	/**
	 * @param hostAddress the hostAddress to set
	 */
	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	@Override
	public String toString() {
		return "UpdateRequestStatusRequest [status=" + status + ", statusCode="
				+ statusCode + ", serviceId=" + serviceId + ", penaltyCharge="
				+ penaltyCharge + ", processInstanceId=" + processInstanceId
				+ ", hostAddress=" + hostAddress + ", bandwidth=" + bandwidth
				+ "]";
	}

	

}
