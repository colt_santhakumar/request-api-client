package com.colt.novitas.request;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class RequestVLANIdRange implements Comparable<RequestVLANIdRange>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5611516292958685337L;
	
	@SerializedName("from_id_range")
	Integer fromIdRange;
	
	@SerializedName("to_id_range")
	Integer toIdRange;

	public RequestVLANIdRange() {
		super();
	}

	public RequestVLANIdRange(Integer fromIdRange, Integer toIdRange) {
		super();
		this.fromIdRange = fromIdRange;
		this.toIdRange = toIdRange;
	}

	public Integer getFromIdRange() {
		return fromIdRange;
	}

	public void setFromIdRange(Integer fromIdRange) {
		this.fromIdRange = fromIdRange;
	}

	public Integer getToIdRange() {
		return toIdRange;
	}

	public void setToIdRange(Integer toIdRange) {
		this.toIdRange = toIdRange;
	}

	@Override
	public String toString() {
		return "VlanIdRangeRequest [fromIdRange=" + fromIdRange + ", toIdRange=" + toIdRange + "]";
	}

	@Override
	public int compareTo(RequestVLANIdRange o) {
		return this.getFromIdRange().compareTo(o.getFromIdRange());
	}
}
