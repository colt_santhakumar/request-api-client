package com.colt.novitas.request;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;


public class DeleteRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SerializedName("portal_user_id")
	private Integer portalUserId;
	@SerializedName("bcn")
	private String bcn;
	
	@SerializedName("cron_execution")
	private String cronExecution;

	
	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}
	
	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getCronExecution() {
		return cronExecution;
	}

	public void setCronExecution(String cronExecution) {
		this.cronExecution = cronExecution;
	}

	@Override
	public String toString() {
		return "DeleteRequest [portalUserId=" + portalUserId + ", bcn=" + bcn
				+ ", cronExecution=" + cronExecution + "]";
	}
	
	

}
