package com.colt.novitas.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.colt.novitas.client.util.Callback;
import com.colt.novitas.client.util.HttpMethod;
import com.colt.novitas.client.util.HttpUtils;
import com.colt.novitas.request.DeleteRequest;
import com.colt.novitas.request.RequestVLANIdRange;
import com.colt.novitas.request.UpdateRequestStatusRequest;
import com.colt.novitas.response.CloudConnectionRequestResponse;
import com.colt.novitas.response.CloudPortRequestResponse;
import com.colt.novitas.response.GetConnectionRequestResponse;
import com.colt.novitas.response.GetPortRequestResponse;
import com.colt.novitas.response.SitePortRequestDetails;
import com.colt.novitas.response.UpdateRequestStatusResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author CoderSlay
 *
 */
public class RequestAPIClient {
    
    private static final Logger LOG =  LoggerFactory.getLogger(MDC.get("unique_log_id")+"|"+RequestAPIClient.class.getName());
    
    private static final Gson GSON = new Gson();

	private String requestApiUrl = "http://amsnov02:8080/request-api/api/";
	private String requestApiUsername = "NovRequestUser59";
	private String requestApiPassword = "NdxUCCd2Ff32jkve";

	/**
	 * Constructor to assign RequestApiUrl, Username & Password dynamically.
	 * 
	 * @param requestApiUrl
	 * @param requestApiUsername
	 * @param requestApiPassword
	 */
	public RequestAPIClient(String requestApiUrl, String requestApiUsername, String requestApiPassword) {
		super();
		this.requestApiUrl = requestApiUrl;
		this.requestApiUsername = requestApiUsername;
		this.requestApiPassword = requestApiPassword;
	}

	/**
	 * Constructor to assign Username & Password dynamically.
	 * 
	 * @param requestApiUsername
	 * @param requestApiPassword
	 */
	public RequestAPIClient(String requestApiUsername, String requestApiPassword) {
		super();
		this.requestApiUsername = requestApiUsername;
		this.requestApiPassword = requestApiPassword;
	}

	public RequestAPIClient() {
		super();
	}

	
	@SuppressWarnings("rawtypes")
	public Object getPortorConnectionRequest(Integer requestId) {

		String url = requestApiUrl+"/requests/" + requestId;
		
		final List results = new ArrayList();

		    HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, new Callback() {

	            @SuppressWarnings("unchecked")
				@Override
		        public void success(String response, int code) {

		            List request = null;

		            if(response.contains("\"port_id\"")) {
		                // GetPortRequestResponse
		                request = GSON.fromJson(response, new TypeToken<List<GetPortRequestResponse>>(){}.getType());
		                
		            } else if(response.contains("\"from_port_name\"")) {
		                // GetConnectionRequestResponse
		                request = GSON.fromJson(response, new TypeToken<List<GetConnectionRequestResponse>>(){}.getType());
		                
		            }
		            
		            results.addAll(request);
		        }

	            @SuppressWarnings("unchecked")
				@Override
		        public void failure(String response, int code) {
		            if(response.contains("\"status_id\"")) {
		                Object status = GSON.fromJson(response, UpdateRequestStatusResponse.class);
		                
		                results.add(status);
		            }
		        }

		        @Override
		        public void failure(Exception e) {
		        }

		    });
		    
		return results.get(0);

	}
	
	public Object updatePortorConnectionRequest(Integer requestId, UpdateRequestStatusRequest requestObject) {
	    return this.updatePortorConnectionRequest("/requests/", requestId, requestObject);
	}

	private Object updatePortorConnectionRequest(String path, Integer requestId, UpdateRequestStatusRequest requestObject) {
		String url = requestApiUrl + path + requestId;

		HttpClient client = null;
		PutMethod putMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = requestApiUsername + ":" + requestApiPassword;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			// Set JSON request
			JSONObject jsonParam = new JSONObject();
			jsonParam.put("status", requestObject.getStatus());
			jsonParam.put("status_code", requestObject.getStatusCode());
			jsonParam.put("service_id", requestObject.getServiceId());
			jsonParam.put("penalty_charge", requestObject.getPenaltyCharge());
			jsonParam.put("process_instance_id", requestObject.getProcessInstanceId());
			jsonParam.put("host_address", requestObject.getHostAddress());
			jsonParam.put("bandwidth", requestObject.getBandwidth());
			

			// Send PUT request
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");

			putMethod = new PutMethod(url);
			putMethod.setRequestEntity(requestEntity);

			// Add Headers
			putMethod.addRequestHeader("Content-Type", "application/json");
			putMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);
			putMethod.addRequestHeader("x-NovitasApiId", MDC.get("unique_log_id"));
			int responseCode = client.executeMethod(putMethod);
			LOG.info("\nSending 'PUT' request to URL : " + url);
			LOG.info("PUT parameters : " + jsonParam);
			LOG.info("Response Code : " + responseCode);

			if (responseCode == 200) {
				correctResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				correctResponseReader.close();

				// Print result
				LOG.info(response.toString());
			} else if (responseCode == 204) {
				LOG.info("Successfully changed the Status");
				UpdateRequestStatusResponse updateRequestStatusResponse = new UpdateRequestStatusResponse();
				updateRequestStatusResponse.setStatusId(1);
				return updateRequestStatusResponse;
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				// Print Error result
				LOG.info(response.toString());
				UpdateRequestStatusResponse updateRequestStatusResponse = new UpdateRequestStatusResponse();
				JSONObject jsonObj = new JSONObject(response.toString());
				if (!jsonObj.get("status_id").toString().equals("null")) {
					updateRequestStatusResponse.setStatusId(jsonObj.getInt("status_id"));
				}

				return updateRequestStatusResponse;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return new Object();
	}
	
	private String getPortUrl(String baseUrl, String status, String action) {
	    String url = null;

        if (status.equals("") && !action.equals("")) {
            url = baseUrl + "port?status=" + status;
        } else if (!status.equals("") && action.equals("")) {
            url = baseUrl + "port?action=" + action;
        } else {
            url = baseUrl + "port?status=" + status + "&action=" + action;
        }
        
        return url;
	}

	public List<GetPortRequestResponse> getAllPortRequests(String status, String action) {

		String url = null;

		if (status.equals("") && !action.equals("")) {
			url = requestApiUrl + "/requests/" + "port?status=" + status;
		} else if (!status.equals("") && action.equals("")) {
			url = requestApiUrl + "/requests/" + "port?action=" + action;
		} else {
			url = requestApiUrl + "/requests/" + "port?status=" + status + "&action=" + action;
		}

		final List<GetPortRequestResponse> portResponseList = new ArrayList<GetPortRequestResponse>();

		HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, new Callback() {

			@Override
			public void success(String response, int code) {

				List<GetPortRequestResponse> portResps = GSON.fromJson(response,
						new TypeToken<List<GetPortRequestResponse>>() {
						}.getType());
				portResponseList.addAll(portResps);

			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});

		return portResponseList;
	}

	private String getConnectionUrl(String baseUrl, List<String> status, List<String> action) {
	    String url = null;

        if (null != status && !status.isEmpty() ) {
            for (String stat : status) {
            	url = baseUrl + "?status=" + stat;
			}
        } 
        
        if (null != action && !action.isEmpty() ) {
            for (String act : action) {
            	url = baseUrl + "?action=" + act;
			}
        }
        return url;
	}
	
	public List<GetConnectionRequestResponse> getAllConnectionRequests(List<String> status, List<String> action) {

		String url = requestApiUrl + "/requests/" + "connection";
		if (null != status && !status.isEmpty()) {
			for (String stat : status) {
				 url = url+"?status=" + stat;
			}
		} 
		if (null != action && !action.isEmpty()) {
			for (String act : action) {
				 url = url+"?action=" + act;
			}
		} 

		final List<GetConnectionRequestResponse> connectionResponseList = new ArrayList<GetConnectionRequestResponse>();

		HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, new Callback() {

			@Override
			public void success(String response, int code) {

				List<GetConnectionRequestResponse> portResps = GSON.fromJson(response,
						new TypeToken<List<GetConnectionRequestResponse>>() {
						}.getType());
				connectionResponseList.addAll(portResps);

			}

			@Override
			public void failure(String response, int code) {
			}

			@Override
			public void failure(Exception e) {
			}

		});

		return connectionResponseList;
	}
	
	public String getNovitasSiteType(String pmCompositeSiteType){
		String url = requestApiUrl + "/location/sitetype" ;

		HttpClient client = null;
		PostMethod postMethod = null;
		BufferedReader correctResponseReader = null;
		BufferedReader errorResponseReader = null;

		try {
			client = new HttpClient();

			String authString = requestApiUsername + ":" + requestApiPassword;
			byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			JSONObject jsonParam = new JSONObject();
			jsonParam.put("pm_composite_site_type", pmCompositeSiteType);
			StringRequestEntity requestEntity = new StringRequestEntity(jsonParam.toString(), "application/json",
					"UTF-8");
			
			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(requestEntity);

			// Add Headers
			postMethod.addRequestHeader("Content-Type", "application/json");
			postMethod.addRequestHeader("Authorization", "Basic " + authStringEnc);
			postMethod.addRequestHeader("x-NovitasApiId", MDC.get("unique_log_id"));
			int responseCode = client.executeMethod(postMethod);

			LOG.info("\nSending 'POST' request to URL : " + url);
			LOG.info("POST parameters : " + jsonParam);
			LOG.info("Response Code : " + responseCode);


			if (responseCode == 200) {

				correctResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));

				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = correctResponseReader.readLine()) != null) {
					response.append(inputLine);
				}

				correctResponseReader.close();

				// Print result
				LOG.info(response.toString());
				return response.toString();
				
			} else {
				errorResponseReader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = errorResponseReader.readLine()) != null) {
					response.append(inputLine);
				}
				errorResponseReader.close();

				// Print Error result
				LOG.info(response.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (correctResponseReader != null) {
					correctResponseReader.close();
				}

				if (errorResponseReader != null) {
					errorResponseReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		
		return "";
	}

	@SuppressWarnings("unused")
	private List<RequestVLANIdRange> getVLANRanges(final JSONArray ranges) throws JSONException {
		final List<RequestVLANIdRange> vlanRange = new ArrayList<RequestVLANIdRange>();
		if (ranges != null && ranges.length() > 0) {
			for (int i = 0; i < ranges.length(); i++) {
				final JSONObject rangeObject = ranges.getJSONObject(i);

				Integer fromIdRange = null;
				try {
					fromIdRange = rangeObject.getInt("from_id_range");
				} catch (final Exception e1) {
				}
				Integer toIdRange = null;
				try {
					toIdRange = rangeObject.getInt("to_id_range");
				} catch (final Exception e) {
				}
				vlanRange.add(new RequestVLANIdRange(fromIdRange, toIdRange));
			}
		}
		return vlanRange;
	}
	
	/**
	 * Delete port request
	 * @param portId
	 * @param customerId
	 * @param request
	 * @return
	 */
	public String deletePort(String portId, int customerId, DeleteRequest request) {
		
		final StringBuilder status = new StringBuilder();
		
		String url = this.requestApiUrl + "/requests/port/" + portId + "?customerid=" + customerId;
        
        String body = GSON.toJson(request);

        HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, HttpMethod.POST, body, new Callback() {

            @Override
            public void success(String response, int code) {
            	status.append(response);
            }

            @Override
            public void failure(String response, int code) {
            }

            @Override
            public void failure(Exception e) {                
            }
            
        });
        
        return status.toString();
		
	}
	
	// ------------------------------------------------------- Cloud connections and ports -----------------------------------

	/**
	 * 
	 * Get all cloud connections
	 * @param status
	 * @param action
	 * @return
	 */
	public List<CloudConnectionRequestResponse> getAllCloudConnectionRequests(List<String> status, List<String> action) {
	    
	    String url = this.getConnectionUrl(requestApiUrl + "/cloudrequests/connection", status, action);
	    
	    final List<CloudConnectionRequestResponse> connections = new ArrayList<CloudConnectionRequestResponse>();

	    HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, new Callback() {

	        @Override
	        public void success(String response, int code) {

	            //JSONObject jsonObj = new JSONObject(response.toString());

	            List<CloudConnectionRequestResponse> conns = 
	                    GSON.fromJson(response, new TypeToken<List<CloudConnectionRequestResponse>>(){}.getType());
	            connections.addAll(conns);

	        }

	        @Override
	        public void failure(String response, int code) {                
	        }

	        @Override
	        public void failure(Exception e) {
	        }

	    });
	    
	    return connections;
	}

	/**
	 * Get a request by Id
	 * @param requestId
	 * @return
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
    public Object getCloudPortOrConnectionRequest(Integer requestId) {
	    
	    String url = requestApiUrl + "/cloudrequests/" + requestId;
	    final List results = new ArrayList();

	    HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, new Callback() {

            @Override
	        public void success(String response, int code) {

	            List request = null;

	            if(response.contains("\"port_id\"")) {
	                // CloudPortRequestResponse
	                request = GSON.fromJson(response, new TypeToken<List<CloudPortRequestResponse>>(){}.getType());
	                
	            } else if(response.contains("\"component_connections\"")) {
	                // CloudConnectionRequestResponse
	                request = GSON.fromJson(response, new TypeToken<List<CloudConnectionRequestResponse>>(){}.getType());
	                
	            }
	            
	            results.addAll(request);
	        }

            @Override
	        public void failure(String response, int code) {
	            if(response.contains("\"status_id\"")) {
	                Object status = GSON.fromJson(response, UpdateRequestStatusResponse.class);
	                
	                results.add(status);
	            }
	        }

	        @Override
	        public void failure(Exception e) {
	        }

	    });
        
	    return results.get(0);
	}
	
	/**
	 * Get all cloud ports
	 * @param status
	 * @param action
	 * @return
	 */
	public List<CloudPortRequestResponse> getAllCloudPortRequests(String status, String action) {
	    
	    String url = this.getPortUrl(requestApiUrl + "/cloudrequests/", status, action);
	    
	    final List<CloudPortRequestResponse> ports = new ArrayList<CloudPortRequestResponse>();

	    HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, new Callback() {

            @Override
            public void success(String response, int code) {

                List<CloudPortRequestResponse> portResps = 
                        GSON.fromJson(response, new TypeToken<List<CloudPortRequestResponse>>(){}.getType());
                ports.addAll(portResps);

            }

            @Override
            public void failure(String response, int code) {                
            }

            @Override
            public void failure(Exception e) {
            }

        });
	    
	    return ports;
	}
	
	/**
	 * Update cloud connection or port status
	 * @param requestId
	 * @param requestObject
	 * @return
	 */
	public Object updateCloudPortOrConnectionRequest(Integer requestId, UpdateRequestStatusRequest requestObject) {
	    return this.updatePortorConnectionRequest("/cloudrequests/", requestId, requestObject);
	}
	


	public void  refreshProductCache(String siteId, int minutes) {

		String url = requestApiUrl + "/product?service_type=ETHERNETPORT&location_id=" + encodeString(siteId);
		CloseableHttpAsyncClient client = HttpAsyncClients.createDefault();
		client.start ();
		HttpGet getMethod = new HttpGet(url);
		// Set Credentials
		String authString = requestApiUsername + ":" + requestApiPassword;
		byte[] authEncBytes = Base64.encodeBase64((byte[]) authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		// Add Headers
		getMethod.addHeader("Content-Type", "application/json");
		getMethod.addHeader("Authorization", "Basic " + authStringEnc);
		getMethod.addHeader("cache-refresh", String.valueOf(minutes));
		getMethod.addHeader("x-NovitasApiId", MDC.get("unique_log_id"));
		Future<HttpResponse> future = client.execute(getMethod,null);
		LOG.info("\nSending 'GET' request to URL : " + url);
         try{
             LOG.info("Refresh Cache Response"+future.get(1000, TimeUnit.MILLISECONDS));
            }catch(TimeoutException | InterruptedException | ExecutionException e){
              LOG.info("Exception occured"+e);	
		    }
	}


	public static String encodeString(String value) {

		String encodedValue = null;
		try {
			encodedValue = URLEncoder.encode(value.replaceAll("/", "||"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodedValue;

	}

	public static String decodeString(String value) {

		String decodedValue = null;
		try {
			decodedValue = URLDecoder.decode(value, "UTF-8").replace("||", "/");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decodedValue;
	}
	
	/**
	 * Get a request by Id
	 * @param requestId
	 * @return
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
    public List<SitePortRequestDetails> getSitePortRequest(Integer customerId) {
	    
	    String url = requestApiUrl + "/siteport?customerid=" + customerId;
	    final List<SitePortRequestDetails> results = new ArrayList();

	    HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, new Callback() {

            @Override
	        public void success(String response, int code) {
              List<SitePortRequestDetails> request = GSON.fromJson(response, new TypeToken<List<SitePortRequestDetails>>(){}.getType());
	          results.addAll(request);
	        }

            @Override
	        public void failure(String response, int code) {
	           
	        }

	        @Override
	        public void failure(Exception e) {
	        }

	    });
        
	    return results;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
    public List<SitePortRequestDetails> getAllSitePortRequests(String status, String action ) {
	    
	    String url = requestApiUrl + "/siteport?status=" + status+"&action="+action;
	    final List<SitePortRequestDetails> results = new ArrayList();

	    HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, new Callback() {

            @Override
	        public void success(String response, int code) {
              List<SitePortRequestDetails> request = GSON.fromJson(response, new TypeToken<List<SitePortRequestDetails>>(){}.getType());
	          results.addAll(request);
	        }

            @Override
	        public void failure(String response, int code) {
	           
	        }

	        @Override
	        public void failure(Exception e) {
	        }

	    });
        
	    return results;
	}
	
	/**
	 * Delete port request
	 * @param portId
	 * @param customerId
	 * @param request
	 * @return
	 */
	public String deleteCloudPort(String portId, int customerId, DeleteRequest request) {
		
		final StringBuilder status = new StringBuilder();
		
		String url = this.requestApiUrl + "/cloudrequests/port/" + portId + "?customerid=" + customerId;
        
        String body = GSON.toJson(request);

        HttpUtils.connect(url, this.requestApiUsername, this.requestApiPassword, HttpMethod.POST, body, new Callback() {

            @Override
            public void success(String response, int code) {
            	status.append(response);
            }

            @Override
            public void failure(String response, int code) {
            }

            @Override
            public void failure(Exception e) {                
            }
            
        });
        
        return status.toString();
		
	}	
}
