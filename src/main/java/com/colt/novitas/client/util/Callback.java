package com.colt.novitas.client.util;

/**
 * A call back interface for the connect method
 * @author omerio
 *
 */
public interface Callback {

    void success(String response, int code);

    void failure(String response, int code);

    void failure(Exception e);
}