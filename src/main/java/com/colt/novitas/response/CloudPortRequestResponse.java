package com.colt.novitas.response;


import com.google.gson.annotations.SerializedName;

public class CloudPortRequestResponse extends CommonCloudRequestResponse {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6485244331105272073L;

	@SerializedName("port_id")
	private String portId;
    
    @SerializedName("port_name")
    private String portName;
    
    @SerializedName("location_id")
    private String locationId;
    
    @SerializedName("location_name")
    private String locationName;
    //private String connector;
    
    @SerializedName("product_name")
    private String productName;
    
    @SerializedName("last_updated")
    private String lastStatus;  
    
    @SerializedName("penalty")
	private Float penalty;
    
    @SerializedName("penalty_currency")
    private String penaltyCurrency;
    
    @SerializedName("site_type")
	private String siteType;
    
    @SerializedName("site_floor")
	private String siteFloor;
    
    @SerializedName("site_room_name")
	private String siteRoomName;
    
    @SerializedName("location_premises_number")
	private String locationPremisesNumber;
    
    @SerializedName("location_building_name")
	private String locationBuildingName;
    
    @SerializedName("location_street_name")
	private String locationStreetName;
    
    @SerializedName("location_city")
	private String locationCity;
	
	@SerializedName("location_state")
	private String locationState;
	
	@SerializedName("location_country")
	private String locationCountry;
	
	@SerializedName("postal_zip_code")
	private String postalZipCode;
	
	@SerializedName("latitude")
	private Float latitude;
	
	@SerializedName("longitude")
	private Float longitude;
	
	@SerializedName("cloud_provider")
	private String cloudProvider;
	
	@SerializedName("azure_service_key")
	private String serviceKey;
	
	@SerializedName("cron_execution")
	private String cronExecution;
	
	/*@SerializedName("expiration_period") 
	private Integer expirationPeriod;*/

	public CloudPortRequestResponse() {
		super();
	}
	
	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

	public Float getPenalty() {
		return penalty;
	}

	public void setPenalty(Float penalty) {
		this.penalty = penalty;
	}
	
	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	
	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getCloudProvider() {
        return cloudProvider;
    }

    public void setCloudProvider(String cloudProvider) {
        this.cloudProvider = cloudProvider;
    }

	public String getServiceKey() {
		return serviceKey;
	}

	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	public String getCronExecution() {
		return cronExecution;
	}

	public void setCronExecution(String cronExecution) {
		this.cronExecution = cronExecution;
	}
	
	

	/*public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}*/

	@Override
	public String toString() {
		return "CloudPortRequestResponse [portId=" + portId + ", portName="
				+ portName + ", locationId=" + locationId + ", locationName="
				+ locationName + ", productName=" + productName
				+ ", lastStatus=" + lastStatus + ", penalty=" + penalty
				+ ", penaltyCurrency=" + penaltyCurrency + ", siteType="
				+ siteType + ", siteFloor=" + siteFloor + ", siteRoomName="
				+ siteRoomName + ", locationPremisesNumber="
				+ locationPremisesNumber + ", locationBuildingName="
				+ locationBuildingName + ", locationStreetName="
				+ locationStreetName + ", locationCity=" + locationCity
				+ ", locationState=" + locationState + ", locationCountry="
				+ locationCountry + ", postalZipCode=" + postalZipCode
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", cloudProvider=" + cloudProvider + ", serviceKey="
				+ serviceKey + ", cronExecution=" + cronExecution+ "]";
	}

    
}