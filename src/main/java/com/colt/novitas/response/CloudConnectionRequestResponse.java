package com.colt.novitas.response;

import java.util.List;

import com.colt.novitas.request.ConnectionPortPair;
import com.google.gson.annotations.SerializedName;

public class CloudConnectionRequestResponse extends CommonCloudRequestResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2802524101036844607L;

	// private String fromPort;
	// private String toPort;
	// private String fromPortName;
	// private String toPortName;
	@SerializedName("last_updated")
	private String lastUpdated;

	@SerializedName("price_id")
	private Integer priceId;

	@SerializedName("connection_name")
	private String connectionName;

	@SerializedName("connection_id")
	private String connectionId;

	@SerializedName("old_bandwidth")
	private Integer oldBandwidth;

	@SerializedName("old_rental_unit")
	private String oldRentalUnit;

	@SerializedName("old_rental_charge")
	private Float oldRentalCharge;

	@SerializedName("old_rental_currency")
	private String oldRentalCurrency;

	@SerializedName("modification_charge")
	private Float modificationCharge;

	@SerializedName("modification_currency")
	private String modificationCurrency;
	/*
	 * private String fromVlanMapping; private String toVlanMapping; private
	 * String fromVlanType; private String toVlanType; private
	 * List<VlanIdRangeRequest> fromPortVLANIdRange; private
	 * List<VlanIdRangeRequest> toPortVLANIdRange;
	 */
	@SerializedName("penalty_charge")
	private Float penaltyCharge;

	@SerializedName("coterminus_option")
	private Boolean coterminusOption;

	@SerializedName("component_connections")
	private List<ConnectionPortPair> ports;

	@SerializedName("cloud_provider")
	private String cloudProvider;
	
	@SerializedName("cron_execution")
	private String cronExecution;

	public CloudConnectionRequestResponse() {
		super();
	}


	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Integer getPriceId() {
		return priceId;
	}

	public void setPriceId(Integer priceId) {
		this.priceId = priceId;
	}

	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public Integer getOldBandwidth() {
		return oldBandwidth;
	}

	public void setOldBandwidth(Integer oldBandwidth) {
		this.oldBandwidth = oldBandwidth;
	}

	public String getOldRentalUnit() {
		return oldRentalUnit;
	}

	public void setOldRentalUnit(String oldRentalUnit) {
		this.oldRentalUnit = oldRentalUnit;
	}

	public Float getOldRentalCharge() {
		return oldRentalCharge;
	}

	public void setOldRentalCharge(Float oldRentalCharge) {
		this.oldRentalCharge = oldRentalCharge;
	}

	public String getOldRentalCurrency() {
		return oldRentalCurrency;
	}

	public void setOldRentalCurrency(String oldRentalCurrency) {
		this.oldRentalCurrency = oldRentalCurrency;
	}

	public Float getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(Float modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	public String getModificationCurrency() {
		return modificationCurrency;
	}

	public void setModificationCurrency(String modificationCurrency) {
		this.modificationCurrency = modificationCurrency;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public Boolean getCoterminusOption() {
		return coterminusOption;
	}

	public void setCoterminusOption(Boolean coterminusOption) {
		this.coterminusOption = coterminusOption;
	}

	public List<ConnectionPortPair> getPorts() {
		return ports;
	}

	public void setPorts(List<ConnectionPortPair> ports) {
		this.ports = ports;
	}

	public String getCloudProvider() {
		return cloudProvider;
	}

	public void setCloudProvider(String cloudProvider) {
		this.cloudProvider = cloudProvider;
	}


	public String getCronExecution() {
		return cronExecution;
	}


	public void setCronExecution(String cronExecution) {
		this.cronExecution = cronExecution;
	}


	@Override
	public String toString() {
		return "CloudConnectionRequestResponse [lastUpdated=" + lastUpdated
				+ ", priceId=" + priceId + ", connectionName=" + connectionName
				+ ", connectionId=" + connectionId + ", oldBandwidth="
				+ oldBandwidth + ", oldRentalUnit=" + oldRentalUnit
				+ ", oldRentalCharge=" + oldRentalCharge
				+ ", oldRentalCurrency=" + oldRentalCurrency
				+ ", modificationCharge=" + modificationCharge
				+ ", modificationCurrency=" + modificationCurrency
				+ ", penaltyCharge=" + penaltyCharge + ", coterminusOption="
				+ coterminusOption + ", ports=" + ports + ", cloudProvider="
				+ cloudProvider + ", cronExecution=" + cronExecution + "]";
	}

    

}