package com.colt.novitas.response;

import com.google.gson.annotations.SerializedName;



public class SitePortContact {
	
	@SerializedName("site")
	private SiteContact siteContact;
	
	@SerializedName("technical")
	private SiteContact techniContact;

	public SiteContact getSiteContact() {
		return siteContact;
	}

	public void setSiteContact(SiteContact siteContact) {
		this.siteContact = siteContact;
	}

	public SiteContact getTechniContact() {
		return techniContact;
	}

	public void setTechniContact(SiteContact techniContact) {
		this.techniContact = techniContact;
	}

	@Override
	public String toString() {
		return "SitePortContact [siteContact=" + siteContact
				+ ", techniContact=" + techniContact + "]";
	}
    
	
	
	
	

}
