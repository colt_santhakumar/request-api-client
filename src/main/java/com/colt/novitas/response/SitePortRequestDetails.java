package com.colt.novitas.response;

import java.io.Serializable;
import java.util.Set;

import com.google.gson.annotations.SerializedName;

/**
 * @author drao
 *
 */

public class SitePortRequestDetails implements Serializable {

	private static final long serialVersionUID = 1854641500423030247L;
    
	
	@SerializedName("action")
	private String action;
	
	@SerializedName("id")
	private Integer requestId;
	
	@SerializedName("location")
	private String location;
	
	@SerializedName("address")
	private String address; //"900 Coronation Road, NW10 7QP. London, United Kingdom.",
	
	@SerializedName("commitment_period")
	private Integer commitmentPeriod;
	
	@SerializedName("customer_name")
	private String customerName;
	
	@SerializedName("last_updated")
	private String lastUpdated;
	
	@SerializedName("requested_at")
	private String requestedAt;
	
	@SerializedName("latitude")
	private Float latitude;
	
	@SerializedName("location_building_name")
	private String locationBuildingName;
	
	@SerializedName("location_premises_number")
	private String locationPremisesNumber;
	
	@SerializedName("location_city")
	private String locationCity;
	
	@SerializedName("location_country")
	private String locationCountry;
	
	@SerializedName("location_id")
	private String locationId;
	
	@SerializedName("location_street_name")
	private String locationStreetName;
	
	@SerializedName("longitude")
	private Float longitude;
	
	@SerializedName("location_name")
	private String locationName;
	
	@SerializedName("installation_charge")
	private Float installationCharge;
	
	@SerializedName("installation_currency")
	private String installationCurrency;
	
	@SerializedName("ocn")
	private String ocn;
	
	@SerializedName("bcn")
	private String bcn;
	
	@SerializedName("customer_id")
	private String customerId;
	
	@SerializedName("portal_user_id")
	private Integer portalUserId;
	
	@SerializedName("service_id")
	private String serviceId;
	
	@SerializedName("crd")
	private String customerRequiredDate;
	
    @SerializedName("cpd")
    private String customerPromisedDate;
    
    @SerializedName("postal_zip_code")
    private String postalZipCode;
    
    @SerializedName("site_floor")
    private String siteFloor;
    
    @SerializedName("site_room_name")
    private String siteRoomName;
    
    @SerializedName("site_type")
    private String siteType;
    
    @SerializedName("site_access_out_of_hours")
    private boolean siteAccessOutOfHours;
    
    @SerializedName("status")
    private String status;
    
    @SerializedName("status_code")
    private Integer statusCode;
    
    @SerializedName("status_code_description")
    private String statusDescription;
    
    @SerializedName("order_id")
    private String orderId;
    
    @SerializedName("order_status")
    private String orderStatus;
    
    @SerializedName("order_status_updated")
    private String orderStatusUpdated;
    
    @SerializedName("contacts")
    private SitePortContact contacts;
    
	@SerializedName("ports")
	private Set<SitePortRequestPortDetails> ports;
	
	@SerializedName("process_instance_id")
    private String processInstanceId;
	
	@SerializedName("cron_execution")
	private String cronExecution;
    
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	@SerializedName("host_address")
    private String hostAddress;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress() {
		return  locationPremisesNumber+","+locationStreetName+","+postalZipCode+","+locationCity+","+locationCountry;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public boolean isSiteAccessOutOfHours() {
		return siteAccessOutOfHours;
	}

	public void setSiteAccessOutOfHours(boolean siteAccessOutOfHours) {
		this.siteAccessOutOfHours = siteAccessOutOfHours;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public SitePortContact getContacts() {
		return contacts;
	}

	public void setContacts(SitePortContact contacts) {
		this.contacts = contacts;
	}

	public Set<SitePortRequestPortDetails> getPorts() {
		return ports;
	}

	public void setPorts(Set<SitePortRequestPortDetails> ports) {
		this.ports = ports;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}
     
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}
	
	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public String getCustomerRequiredDate() {
		return customerRequiredDate;
	}

	public void setCustomerRequiredDate(String customerRequiredDate) {
		this.customerRequiredDate = customerRequiredDate;
	}

	public String getCustomerPromisedDate() {
		return customerPromisedDate;
	}

	public void setCustomerPromisedDate(String customerPromisedDate) {
		this.customerPromisedDate = customerPromisedDate;
	}

	public String getOrderStatusUpdated() {
		return orderStatusUpdated;
	}

	public void setOrderStatusUpdated(String orderStatusUpdated) {
		this.orderStatusUpdated = orderStatusUpdated;
	}

	public String getCronExecution() {
		return cronExecution;
	}

	public void setCronExecution(String cronExecution) {
		this.cronExecution = cronExecution;
	}

	@Override
	public String toString() {
		return "SitePortRequestDetails [action=" + action + ", requestId="
				+ requestId + ", location=" + location + ", address=" + address
				+ ", commitmentPeriod=" + commitmentPeriod + ", customerName="
				+ customerName + ", lastUpdated=" + lastUpdated
				+ ", requestedAt=" + requestedAt + ", latitude=" + latitude
				+ ", locationBuildingName=" + locationBuildingName
				+ ", locationPremisesNumber=" + locationPremisesNumber
				+ ", locationCity=" + locationCity + ", locationCountry="
				+ locationCountry + ", locationId=" + locationId
				+ ", locationStreetName=" + locationStreetName + ", longitude="
				+ longitude + ", locationName=" + locationName
				+ ", installationCharge=" + installationCharge
				+ ", installationCurrency=" + installationCurrency + ", ocn="
				+ ocn + ", bcn=" + bcn + ", customerId=" + customerId
				+ ", portalUserId=" + portalUserId + ", serviceId=" + serviceId
				+ ", customerRequiredDate=" + customerRequiredDate
				+ ", customerPromisedDate=" + customerPromisedDate
				+ ", postalZipCode=" + postalZipCode + ", siteFloor="
				+ siteFloor + ", siteRoomName=" + siteRoomName + ", siteType="
				+ siteType + ", siteAccessOutOfHours=" + siteAccessOutOfHours
				+ ", status=" + status + ", statusCode=" + statusCode
				+ ", statusDescription=" + statusDescription + ", orderId="
				+ orderId + ", orderStatus=" + orderStatus
				+ ", orderStatusUpdated=" + orderStatusUpdated + ", contacts="
				+ contacts + ", ports=" + ports + ", processInstanceId="
				+ processInstanceId + ", cronExecution=" + cronExecution
				+ ", hostAddress=" + hostAddress + "]";
	}

    

	
}