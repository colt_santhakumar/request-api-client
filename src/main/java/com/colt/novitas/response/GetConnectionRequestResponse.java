package com.colt.novitas.response;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.request.RequestVLANIdRange;
import com.google.gson.annotations.SerializedName;

public class GetConnectionRequestResponse implements Serializable {

	private static final long serialVersionUID = -3889856842640778210L;

	@SerializedName("id") private Integer id;
	@SerializedName("service_id") private String serviceId;
	@SerializedName("portal_user_id") private Integer portalUserId;
	@SerializedName("action") private String action;
	@SerializedName("status") private String status;
	@SerializedName("from_port") private String fromPort;
	@SerializedName("to_port") private String toPort;
	@SerializedName("from_port_name") private String fromPortName;
	@SerializedName("to_port_name") private String toPortName;
	@SerializedName("bandwidth") private Integer bandwidth;
	@SerializedName("description") private String description;
	@SerializedName("requested_at") private String requestedAt;
	@SerializedName("last_updated") private String lastUpdated;
	@SerializedName("rental_charge") private Float rentalCharge;
	@SerializedName("rental_unit") private String rentalUnit;
	@SerializedName("rental_currency") private String rentalCurrency;
	@SerializedName("price_id") private Integer priceId;
	@SerializedName("connection_name") private String connectionName;
	@SerializedName("installation_charge") private Float installationCharge;
	@SerializedName("installation_currency") private String installationCurrency;
	@SerializedName("connection_id") private String connectionId;
	@SerializedName("old_bandwidth") private Integer oldBandwidth;
	@SerializedName("old_rental_unit") private String oldRentalUnit;
	@SerializedName("old_rental_charge") private Float oldRentalCharge;
	@SerializedName("old_rental_currency") private String oldRentalCurrency;
	@SerializedName("modification_charge") private Float modificationCharge;
	@SerializedName("modification_currency") private String modificationCurrency;
	@SerializedName("decommissioning_charge") private Float decommissioningCharge;
	@SerializedName("decommissioning_currency") private String decommissioningCurrency;
	@SerializedName("status_code") private Integer statusCode;
	@SerializedName("status_code_description") private String statusCodeDescription;
	@SerializedName("bcn") private String bcn;
	@SerializedName("a_end_vlan_mapping") private String fromVlanMapping;
	@SerializedName("b_end_vlan_mapping") private String toVlanMapping;
	@SerializedName("a_end_vlan_type") private String fromVlanType;
	@SerializedName("b_end_vlan_type") private String toVlanType;
	@SerializedName("a_end_vlan_ids") private List<RequestVLANIdRange> fromPortVLANIdRange;
	@SerializedName("b_end_vlan_ids") private List<RequestVLANIdRange> toPortVLANIdRange;
	@SerializedName("ocn") private String ocn;
	@SerializedName("customer_name") private String customerName;
	@SerializedName("penalty_charge") private Float penaltyCharge; 
	@SerializedName("commitment_period") private Integer commitmentPeriod; 
	@SerializedName("coterminus_option") private Boolean coterminusOption;
	
	@SerializedName("cron_execution") private String cronExecution;

	@SerializedName("depends_on_request_ids")
	private String dependsOnRequestIds;

	@SerializedName("has_dependent_requests")
	private Boolean hasDependentRequests;

	public GetConnectionRequestResponse() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFromPort() {
		return fromPort;
	}

	public void setFromPort(String fromPort) {
		this.fromPort = fromPort;
	}

	public String getToPort() {
		return toPort;
	}

	public void setToPort(String toPort) {
		this.toPort = toPort;
	}

	public String getFromPortName() {
		return fromPortName;
	}

	public void setFromPortName(String fromPortName) {
		this.fromPortName = fromPortName;
	}

	public String getToPortName() {
		return toPortName;
	}

	public void setToPortName(String toPortName) {
		this.toPortName = toPortName;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public Integer getPriceId() {
		return priceId;
	}

	public void setPriceId(Integer priceId) {
		this.priceId = priceId;
	}

	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public Integer getOldBandwidth() {
		return oldBandwidth;
	}

	public void setOldBandwidth(Integer oldBandwidth) {
		this.oldBandwidth = oldBandwidth;
	}

	public String getOldRentalUnit() {
		return oldRentalUnit;
	}

	public void setOldRentalUnit(String oldRentalUnit) {
		this.oldRentalUnit = oldRentalUnit;
	}

	public Float getOldRentalCharge() {
		return oldRentalCharge;
	}

	public void setOldRentalCharge(Float oldRentalCharge) {
		this.oldRentalCharge = oldRentalCharge;
	}

	public String getOldRentalCurrency() {
		return oldRentalCurrency;
	}

	public void setOldRentalCurrency(String oldRentalCurrency) {
		this.oldRentalCurrency = oldRentalCurrency;
	}

	public Float getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(Float modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	public String getModificationCurrency() {
		return modificationCurrency;
	}

	public void setModificationCurrency(String modificationCurrency) {
		this.modificationCurrency = modificationCurrency;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusCodeDescription() {
		return statusCodeDescription;
	}

	public void setStatusCodeDescription(String statusCodeDescription) {
		this.statusCodeDescription = statusCodeDescription;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getFromVlanMapping() {
		return fromVlanMapping;
	}

	public void setFromVlanMapping(String fromVlanMapping) {
		this.fromVlanMapping = fromVlanMapping;
	}

	public String getToVlanMapping() {
		return toVlanMapping;
	}

	public void setToVlanMapping(String toVlanMapping) {
		this.toVlanMapping = toVlanMapping;
	}

	public String getFromVlanType() {
		return fromVlanType;
	}

	public void setFromVlanType(String fromVlanType) {
		this.fromVlanType = fromVlanType;
	}

	public String getToVlanType() {
		return toVlanType;
	}

	public void setToVlanType(String toVlanType) {
		this.toVlanType = toVlanType;
	}

	public List<RequestVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(
			List<RequestVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<RequestVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<RequestVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	/**
	 * @return the penaltyCharge
	 */
	public Float getPenaltyCharge() {
		return penaltyCharge;
	}


	/**
	 * @param penaltyCharge the penaltyCharge to set
	 */
	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}


	/**
	 * @return the commitmentPeriod
	 */
	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}


	/**
	 * @param commitmentPeriod the commitmentPeriod to set
	 */
	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}


	/**
	 * @return the coterminusOption
	 */
	public Boolean getCoterminusOption() {
		return coterminusOption;
	}


	/**
	 * @param coterminusOption the coterminusOption to set
	 */
	public void setCoterminusOption(Boolean coterminusOption) {
		this.coterminusOption = coterminusOption;
	}


	public String getCronExecution() {
		return cronExecution;
	}


	public void setCronExecution(String cronExecution) {
		this.cronExecution = cronExecution;
	}

	public String getDependsOnRequestIds() {
		return dependsOnRequestIds;
	}

	public void setDependsOnRequestIds(String dependsOnRequestIds) {
		this.dependsOnRequestIds = dependsOnRequestIds;
	}

	public Boolean isHasDependentRequests() {
		return hasDependentRequests;
	}

	public void setHasDependentRequests(Boolean hasDependentRequests) {
		this.hasDependentRequests = hasDependentRequests;
	}

	@Override
	public String toString() {
		return "GetConnectionRequestResponse [id=" + id + ", serviceId="
				+ serviceId + ", portalUserId=" + portalUserId + ", action="
				+ action + ", status=" + status + ", fromPort=" + fromPort
				+ ", toPort=" + toPort + ", fromPortName=" + fromPortName
				+ ", toPortName=" + toPortName + ", bandwidth=" + bandwidth
				+ ", description=" + description + ", requestedAt="
				+ requestedAt + ", lastUpdated=" + lastUpdated
				+ ", rentalCharge=" + rentalCharge + ", rentalUnit="
				+ rentalUnit + ", rentalCurrency=" + rentalCurrency
				+ ", priceId=" + priceId + ", connectionName=" + connectionName
				+ ", installationCharge=" + installationCharge
				+ ", installationCurrency=" + installationCurrency
				+ ", connectionId=" + connectionId + ", oldBandwidth="
				+ oldBandwidth + ", oldRentalUnit=" + oldRentalUnit
				+ ", oldRentalCharge=" + oldRentalCharge
				+ ", oldRentalCurrency=" + oldRentalCurrency
				+ ", modificationCharge=" + modificationCharge
				+ ", modificationCurrency=" + modificationCurrency
				+ ", decommissioningCharge=" + decommissioningCharge
				+ ", decommissioningCurrency=" + decommissioningCurrency
				+ ", statusCode=" + statusCode + ", statusCodeDescription="
				+ statusCodeDescription + ", bcn=" + bcn + ", fromVlanMapping="
				+ fromVlanMapping + ", toVlanMapping=" + toVlanMapping
				+ ", fromVlanType=" + fromVlanType + ", toVlanType="
				+ toVlanType + ", fromPortVLANIdRange=" + fromPortVLANIdRange
				+ ", toPortVLANIdRange=" + toPortVLANIdRange + ", ocn=" + ocn
				+ ", customerName=" + customerName + ", penaltyCharge="
				+ penaltyCharge + ", commitmentPeriod=" + commitmentPeriod
				+ ", coterminusOption=" + coterminusOption + ", cronExecution="
				+ cronExecution + ", hasDependantRequests=" + hasDependentRequests
				+ ", dependsOnRequestsIds=" + dependsOnRequestIds + "]";
	}

    

	

}
