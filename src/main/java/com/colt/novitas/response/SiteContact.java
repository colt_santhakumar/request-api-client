package com.colt.novitas.response;

import com.google.gson.annotations.SerializedName;


public class SiteContact {
	
	@SerializedName("site")
	private String siteName;
	@SerializedName("phone")
	private String phone;
	@SerializedName("email")
	private String email;
	
	public SiteContact() {
	}
	
	public SiteContact(String siteName, String phone, String email) {
		super();
		this.siteName = siteName;
		this.phone = phone;
		this.email = email;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "SitePortContactInfo [siteName=" + siteName + ", phone=" + phone
				+ ", email=" + email + "]";
	}

}
