/**
 * 
 */
package com.colt.novitas.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

/**
 * @author omerio
 *
 */
public class CommonCloudRequestResponse implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2575532942046458631L;

	@SerializedName("id")
    protected Integer id;
    
    @SerializedName("service_id")
    protected String serviceId;
    
    @SerializedName("portal_user_id")
    protected Integer portalUserId;
    
    @SerializedName("action")
    protected String action;
    
    @SerializedName("status")
    protected String status;
    
    @SerializedName("bandwidth")
    protected Integer bandwidth;
    
    @SerializedName("requested_at")
    protected String requestedAt;
    
    @SerializedName("rental_charge")
    protected Float rentalCharge;
    
    @SerializedName("rental_unit")
    protected String rentalUnit;
    
    @SerializedName("installation_charge")
    protected Float installationCharge;
    
    @SerializedName("installation_currency")
    protected String installationCurrency;
    
    @SerializedName("decommissioning_charge")
    protected Float decommissioningCharge;
    
    @SerializedName("decommissioning_currency")
    protected String decommissioningCurrency;
    
    @SerializedName("status_code")
    protected Integer statusCode;
    
    @SerializedName("status_code_description")
    protected String statusCodeDescription;
    
    @SerializedName("bcn")
    protected String bcn;
    
    @SerializedName("ocn")
    protected String ocn;
    
    @SerializedName("customer_name")
    protected String customerName;
    
    @SerializedName("commitment_period")
    protected Integer commitmentPeriod;
    
    @SerializedName("rental_currency")
    protected String rentalCurrency;
    
    @SerializedName("description")
    protected String description;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getPortalUserId() {
        return portalUserId;
    }

    public void setPortalUserId(Integer portalUserId) {
        this.portalUserId = portalUserId;
    }
    
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public Integer getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Integer bandwidth) {
        this.bandwidth = bandwidth;
    }
    
    public String getRequestedAt() {
        return requestedAt;
    }

    public void setRequestedAt(String requestedAt) {
        this.requestedAt = requestedAt;
    }

    public Float getRentalCharge() {
        return rentalCharge;
    }

    public void setRentalCharge(Float rentalCharge) {
        this.rentalCharge = rentalCharge;
    }

    public String getRentalCurrency() {
        return rentalCurrency;
    }

    public void setRentalCurrency(String rentalCurrency) {
        this.rentalCurrency = rentalCurrency;
    }

    public String getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(String rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getInstallationCharge() {
        return installationCharge;
    }

    public void setInstallationCharge(Float installationCharge) {
        this.installationCharge = installationCharge;
    }

    public String getInstallationCurrency() {
        return installationCurrency;
    }

    public void setInstallationCurrency(String installationCurrency) {
        this.installationCurrency = installationCurrency;
    }

    public Float getDecommissioningCharge() {
        return decommissioningCharge;
    }

    public void setDecommissioningCharge(Float decommissioningCharge) {
        this.decommissioningCharge = decommissioningCharge;
    }

    public String getDecommissioningCurrency() {
        return decommissioningCurrency;
    }

    public void setDecommissioningCurrency(String decommissioningCurrency) {
        this.decommissioningCurrency = decommissioningCurrency;
    }

    public Integer getCommitmentPeriod() {
        return commitmentPeriod;
    }

    public void setCommitmentPeriod(Integer commitmentPeriod) {
        this.commitmentPeriod = commitmentPeriod;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCodeDescription() {
        return statusCodeDescription;
    }

    public void setStatusCodeDescription(String statusCodeDescription) {
        this.statusCodeDescription = statusCodeDescription;
    }

    public String getBcn() {
        return bcn;
    }

    public void setBcn(String bcn) {
        this.bcn = bcn;
    }

    public String getOcn() {
        return ocn;
    }

    public void setOcn(String ocn) {
        this.ocn = ocn;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    
    
    

}
