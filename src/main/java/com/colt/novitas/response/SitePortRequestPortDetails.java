package com.colt.novitas.response;

import com.google.gson.annotations.SerializedName;


public class SitePortRequestPortDetails {
	
	@SerializedName("id")
	private Integer id;
	
	@SerializedName("port_name")
	private String portName;
	
	@SerializedName("request_id")
	private Integer requestId;
	
	@SerializedName("bandwidth")
	private String bandwidth;
	
	@SerializedName("connector")
	private String connector;
	
	@SerializedName("technology")
	private String technology;
    
	@SerializedName("product_name")
	private String productName;
	
	@SerializedName("installation_charge")
	private Float installationCharge;
	
	@SerializedName("requested_at")
	private String requestedAt;

	@SerializedName("rental_charge")
	private Float rentalCharge;

	@SerializedName("rental_currency")
	private String rentalCurrency;

	@SerializedName("rental_unit")
	private String rentalUnit;

	@SerializedName("installation_vurrency")
	private String installationCurrency;

	@SerializedName("decommissioning_charge")
	private Float decommissioningCharge;

	@SerializedName("decommissioning_currency")
	private String decommissioningCurrency;

	@SerializedName("penalty")
	private Float penalty;

	@SerializedName("penalty_currency")
	private String penaltyCurrency;
    
	@SerializedName("expiration_period")
	private Integer expirationPeriod;
	
	@SerializedName("discount_percentage")
	private Float discountPercentage;

	@SerializedName("customer_port_id")
	private String customerPortID;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public Float getPenalty() {
		return penalty;
	}

	public void setPenalty(Float penalty) {
		this.penalty = penalty;
	}

	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}

	public Float getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Float discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bandwidth == null) ? 0 : bandwidth.hashCode());
		result = prime * result
				+ ((connector == null) ? 0 : connector.hashCode());
		result = prime * result
				+ ((requestId == null) ? 0 : requestId.hashCode());
		result = prime * result
				+ ((technology == null) ? 0 : technology.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SitePortRequestPortDetails other = (SitePortRequestPortDetails) obj;
		if (bandwidth == null) {
			if (other.bandwidth != null)
				return false;
		} else if (!bandwidth.equals(other.bandwidth))
			return false;
		if (connector == null) {
			if (other.connector != null)
				return false;
		} else if (!connector.equals(other.connector))
			return false;
		if (requestId == null) {
			if (other.requestId != null)
				return false;
		} else if (!requestId.equals(other.requestId))
			return false;
		if (technology == null) {
			if (other.technology != null)
				return false;
		} else if (!technology.equals(other.technology))
			return false;
		return true;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public String getCustomerPortID() {
		return customerPortID;
	}

	public void setCustomerPortID(String customerPortID) {
		this.customerPortID = customerPortID;
	}

	@Override
	public String toString() {
		return "SitePortRequestPortDetails [id=" + id + ", portName=" + portName + ", requestId=" + requestId
				+ ", bandwidth=" + bandwidth + ", connector=" + connector + ", technology=" + technology
				+ ", productName=" + productName + ", installationCharge=" + installationCharge + ", requestedAt="
				+ requestedAt + ", rentalCharge=" + rentalCharge + ", rentalCurrency=" + rentalCurrency
				+ ", rentalUnit=" + rentalUnit + ", installationCurrency=" + installationCurrency
				+ ", decommissioningCharge=" + decommissioningCharge + ", decommissioningCurrency="
				+ decommissioningCurrency + ", penalty=" + penalty + ", penaltyCurrency=" + penaltyCurrency
				+ ", expirationPeriod=" + expirationPeriod + ", discountPercentage=" + discountPercentage
				+ ", customerPortID=" + customerPortID + "]";
	}


}
