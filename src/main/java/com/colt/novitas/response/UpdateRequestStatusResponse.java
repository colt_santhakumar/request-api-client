package com.colt.novitas.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class UpdateRequestStatusResponse implements Serializable {

	private static final long serialVersionUID = 1177181499250008548L;

	@SerializedName("status_id")
	private Integer statusId;

	public UpdateRequestStatusResponse() {
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	@Override
	public String toString() {
		return "UpdateRequestStatusResponse [statusId=" + statusId + "]";
	}

}
