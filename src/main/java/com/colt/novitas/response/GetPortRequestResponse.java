package com.colt.novitas.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class GetPortRequestResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7336614319023653591L;
	@SerializedName("id")
	private Integer id;
	@SerializedName("service_id")
	private String serviceId;
	@SerializedName("portal_user_id")
	private Integer portalUserId;
	@SerializedName("action")
	private String action;
	@SerializedName("status")
	private String status;
	@SerializedName("port_id")
	private String portId;
	@SerializedName("port_name")
	private String portName;
	@SerializedName("location_id")
	private String locationId;
	@SerializedName("location_name")
	private String locationName;
	@SerializedName("connector")
	private String connector;
	@SerializedName("product_name")
	private String productName;
	@SerializedName("bandwidth")
	private Integer bandwidth;
	@SerializedName("last_updated")
	private String lastStatus;
	@SerializedName("requested_at")
	private String requestedAt;
	@SerializedName("rental_charge")
	private Float rentalCharge;
	@SerializedName("rental_currency")
	private String rentalCurrency;
	@SerializedName("rental_unit")
	private String rentalUnit;
	@SerializedName("description")
	private String description;
	@SerializedName("installation_charge")
	private Float installationCharge;
	@SerializedName("installation_currency")
	private String installationCurrency;
	@SerializedName("decommissioning_charge")
	private Float decommissioningCharge;
	@SerializedName("decommissioning_currency")
	private String decommissioningCurrency;
	@SerializedName("penalty")
	private Float penalty;
	@SerializedName("penalty_currency")
	private String penaltyCurrency;
	@SerializedName("commitment_period")
	private Integer commitmentPeriod;
	@SerializedName("status_code")
	private Integer statusCode;
	@SerializedName("bcn")
	private String bcn;
	@SerializedName("ocn")
	private String ocn;
	@SerializedName("customer_name")
	private String customerName;
	@SerializedName("site_type")
	private String siteType;
	@SerializedName("site_floor")
	private String siteFloor;
	@SerializedName("site_room_name")
	private String siteRoomName;
	@SerializedName("location_premises_number")
	private String locationPremisesNumber;
	@SerializedName("location_building_name")
	private String locationBuildingName;
	@SerializedName("location_street_name")
	private String locationStreetName;
	@SerializedName("location_city")
	private String locationCity;
	@SerializedName("location_state")
	private String locationState;
	@SerializedName("location_country")
	private String locationCountry;
	@SerializedName("postal_zip_code")
	private String postalZipCode;
	@SerializedName("latitude")
	private Float latitude;
	@SerializedName("longitude")
	private Float longitude;
	@SerializedName("expiration_period")
	private Integer expirationPeriod;
	@SerializedName("loa_allowed")
	private boolean loaAllowed;
	@SerializedName("customer_port_id")
	private String customerPortID;

	@SerializedName("nc_tech_service_id")
	private String ncTechServiceId;

	@SerializedName("ohs_circuit_id")
	private String ohsCircuitId;

	@SerializedName("cron_execution")
	public String cronExecution;

	public GetPortRequestResponse() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public Float getPenalty() {
		return penalty;
	}

	public void setPenalty(Float penalty) {
		this.penalty = penalty;
	}

	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the locationBuildingName
	 */
	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	/**
	 * @param locationBuildingName
	 *            the locationBuildingName to set
	 */
	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	/**
	 * @return the locationStreetName
	 */
	public String getLocationStreetName() {
		return locationStreetName;
	}

	/**
	 * @param locationStreetName
	 *            the locationStreetName to set
	 */
	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	/**
	 * @return the locationCity
	 */
	public String getLocationCity() {
		return locationCity;
	}

	/**
	 * @param locationCity
	 *            the locationCity to set
	 */
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	/**
	 * @return the locationCountry
	 */
	public String getLocationCountry() {
		return locationCountry;
	}

	/**
	 * @param locationCountry
	 *            the locationCountry to set
	 */
	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	/**
	 * @return the latitude
	 */
	public Float getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Float getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the siteType
	 */
	public String getSiteType() {
		return siteType;
	}

	/**
	 * @param siteType
	 *            the siteType to set
	 */
	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	/**
	 * @return the siteFloor
	 */
	public String getSiteFloor() {
		return siteFloor;
	}

	/**
	 * @param siteFloor
	 *            the siteFloor to set
	 */
	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	/**
	 * @return the siteRoomName
	 */
	public String getSiteRoomName() {
		return siteRoomName;
	}

	/**
	 * @param siteRoomName
	 *            the siteRoomName to set
	 */
	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	/**
	 * @return the locationPremisesNumber
	 */
	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	/**
	 * @param locationPremisesNumber
	 *            the locationPremisesNumber to set
	 */
	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	/**
	 * @return the locationState
	 */
	public String getLocationState() {
		return locationState;
	}

	/**
	 * @param locationState
	 *            the locationState to set
	 */
	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	/**
	 * @return the postalZipCode
	 */
	public String getPostalZipCode() {
		return postalZipCode;
	}

	/**
	 * @param postalZipCode
	 *            the postalZipCode to set
	 */
	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}

	public boolean isLoaAllowed() {
		return loaAllowed;
	}

	public void setLoaAllowed(boolean loaAllowed) {
		this.loaAllowed = loaAllowed;
	}

	public String getCronExecution() {
		return cronExecution;
	}

	public void setCronExecution(String cronExecution) {
		this.cronExecution = cronExecution;
	}

	public String getCustomerPortID() {
		return customerPortID;
	}

	public void setCustomerPortID(String customerPortID) {
		this.customerPortID = customerPortID;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public String getOhsCircuitId() {
		return ohsCircuitId;
	}

	public void setOhsCircuitId(String ohsCircuitId) {
		this.ohsCircuitId = ohsCircuitId;
	}

	@Override
	public String toString() {
		return "GetPortRequestResponse [id=" + id + ", serviceId=" + serviceId + ", portalUserId=" + portalUserId
				+ ", action=" + action + ", status=" + status + ", portId=" + portId + ", portName=" + portName
				+ ", locationId=" + locationId + ", locationName=" + locationName + ", connector=" + connector
				+ ", productName=" + productName + ", bandwidth=" + bandwidth + ", lastStatus=" + lastStatus
				+ ", requestedAt=" + requestedAt + ", rentalCharge=" + rentalCharge + ", rentalCurrency="
				+ rentalCurrency + ", rentalUnit=" + rentalUnit + ", description=" + description
				+ ", installationCharge=" + installationCharge + ", installationCurrency=" + installationCurrency
				+ ", decommissioningCharge=" + decommissioningCharge + ", decommissioningCurrency="
				+ decommissioningCurrency + ", penalty=" + penalty + ", penaltyCurrency=" + penaltyCurrency
				+ ", commitmentPeriod=" + commitmentPeriod + ", statusCode=" + statusCode + ", bcn=" + bcn + ", ocn="
				+ ocn + ", customerName=" + customerName + ", siteType=" + siteType + ", siteFloor=" + siteFloor
				+ ", siteRoomName=" + siteRoomName + ", locationPremisesNumber=" + locationPremisesNumber
				+ ", locationBuildingName=" + locationBuildingName + ", locationStreetName=" + locationStreetName
				+ ", locationCity=" + locationCity + ", locationState=" + locationState + ", locationCountry="
				+ locationCountry + ", postalZipCode=" + postalZipCode + ", latitude=" + latitude + ", longitude="
				+ longitude + ", expirationPeriod=" + expirationPeriod + ", loaAllowed=" + loaAllowed
				+ ", customerPortID=" + customerPortID + ", ncTechServiceId=" + ncTechServiceId + ", ohsCircuitId="
				+ ohsCircuitId + ", cronExecution=" + cronExecution + "]";
	}

	
}