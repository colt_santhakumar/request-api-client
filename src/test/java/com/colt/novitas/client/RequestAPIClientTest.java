package com.colt.novitas.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.colt.novitas.request.UpdateRequestStatusRequest;
import com.colt.novitas.response.CloudConnectionRequestResponse;
import com.colt.novitas.response.CloudPortRequestResponse;
import com.colt.novitas.response.UpdateRequestStatusResponse;

/**
 * Integration Test, needs to have a request API server running
 * TODO add support for flushing and adding new request data to the database
 * 
 * @author omerio
 *
 */
@Ignore
public class RequestAPIClientTest {
    
    private RequestAPIClient client;

    @Before
    public void setUp() throws Exception {
        
        client = new RequestAPIClient("http://localhost:8080/request-api/api", "NovRequestUser59", "NdxUCCd2Ff32jkve");
        //client.setRequestCloudApiUrl("http://localhost:8080/request-api/api/cloudrequests/");
    }

    @Test
    public void testGetAllCloudConnectionRequests() {
        
        List<CloudConnectionRequestResponse> requests = client.getAllCloudConnectionRequests(Collections.singletonList("REQUESTED"), Collections.singletonList("CREATE"));
        assertNotNull(requests);
        assertEquals(4, requests.size());
        
        requests = client.getAllCloudConnectionRequests(Collections.singletonList("INPROGRESS"), Collections.singletonList("CREATE"));
        assertNotNull(requests);
        assertEquals(1, requests.size());
    }

    @Test
    public void testGetCloudConnectionRequest() {
        // 10055
        
        Object object = client.getCloudPortOrConnectionRequest(10055);
        
        assertNotNull(object);
        assertTrue(object instanceof CloudConnectionRequestResponse);
        
        CloudConnectionRequestResponse request = (CloudConnectionRequestResponse) object;
        
        assertEquals(new Integer(10055), request.getId());
    }
    
    @Test
    public void testGetCloudPortRequest() {
        // 10056
        
        Object object = client.getCloudPortOrConnectionRequest(10056);
        
        assertNotNull(object);
        assertTrue(object instanceof CloudPortRequestResponse);
        
        CloudPortRequestResponse request = (CloudPortRequestResponse) object;
        
        assertEquals(new Integer(10056), request.getId());
    }
    
    @Test
    public void testGetCloudRequestNotFound() {
        // 10056
        
        Object object = client.getCloudPortOrConnectionRequest(10000056);
        
        assertNotNull(object);
        assertTrue(object instanceof UpdateRequestStatusResponse);
        
        UpdateRequestStatusResponse request = (UpdateRequestStatusResponse) object;
        
        assertEquals(new Integer(-1), request.getStatusId());
    }

    @Test
    public void testGetAllCloudPortRequests() {
        List<CloudPortRequestResponse> requests = client.getAllCloudPortRequests("REQUESTED", "CREATE");
        
        assertNotNull(requests);
        assertEquals(9, requests.size());
        
        requests = client.getAllCloudPortRequests("INPROGRESS", "CREATE");
        
        assertNotNull(requests);
        assertEquals(1, requests.size());
    }

    @Test
    public void testUpdateCloudConnectionRequest() {
        
        // '10040' connection
        Integer requestId = 10040;
        UpdateRequestStatusRequest update = new UpdateRequestStatusRequest();
        update.setStatus("INPROGRESS");
        UpdateRequestStatusResponse response = 
                (UpdateRequestStatusResponse) client.updateCloudPortOrConnectionRequest(requestId, update);
        assertNotNull(response);
        assertEquals(new Integer(-1), response.getStatusId());
        
        
    }
    
    @Test
    public void testUpdateCloudPortRequest() {
        
        // '10041' port
        Integer requestId = 10041;
        UpdateRequestStatusRequest update = new UpdateRequestStatusRequest();
        update.setStatus("INPROGRESS");
        UpdateRequestStatusResponse response = 
                (UpdateRequestStatusResponse) client.updateCloudPortOrConnectionRequest(requestId, update);
        assertNotNull(response);
        assertEquals(new Integer(-1), response.getStatusId());
    }

}
